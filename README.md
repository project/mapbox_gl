CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Configuration
 * Installation
 * Maintainers


INTRODUCTION
------------

This is an API only module. There is no UI. Maps can be generated using
hook_mapbox_gl_info() and calling the render method.

popup should either be "popup" to display the layer properties in a popup,
or the ID of a separate element in which to display the information.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


CONFIGURATION
-------------

No configuration is needed. This is an API only module. There is no UI. Maps
can be generated using hook_mapbox_gl_info() and calling the render method.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


MAINTAINERS
-----------

Current maintainers:
  * Aleix Quintana (aleix) - https://www.drupal.org/u/aleix
  * Ian McLean (imclean) - https://www.drupal.org/u/
